```shell
# either run this to run locally
bundle install
bundle exec middleman server

# OR run this to run with vagrant
vagrant up
```

You can now see the docs at http://localhost:4567. Whoa! That was fast!

_____________________________

Getting Started with Slate
------------------------------

- This was forked from [slate by spectrum.chat]

- [editing Slate markdown](https://github.com/lord/slate/wiki/Markdown-Syntax) 
- [how to publish your docs](https://github.com/lord/slate/wiki/Deploying-Slate)
- [Docker install instructions ](https://github.com/lord/slate/wiki/Docker).

[slate by spectrum.chat]: (https://github.com/lord/slate)
