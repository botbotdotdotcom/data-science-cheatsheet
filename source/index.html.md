---
title: Data Science Cheatsheet

language_tabs: # must be one of https://git.io/vQNgJ
  - shell
  - R
  - python
  - tex

toc_footers:
  - <a href='#'>Built by Phil at botbotdot.com</a>
  - <a href='http://botbotdot.com'>Botbotdot.com - automation</a>
  - <a href='https://github.com/tgrrr/data-science-cheatsheet/tree/master/source/howto/how-to-contribute.md'>Contributions welcome!</a>

includes:
  - stats-notes-final-exam
  - stats-verity-notes
  - data-preprocessing
  
  
search: true
---

### Cheatsheet - Data Science - too long; didn't do

tl;dd: common data science concepts and code

**Why?:**
- Adding AI is easy
- Deeply understanding AI is hard
- These summarise:
  - data science
  - machine learning
  - big data
  - Artificial Intelligence
  - Chatbots and
  - Voice (think Alexa and Google Home)

  **Note:** This is a work in progress. It's literally currently footnotes.
